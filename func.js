const ccxt = require ('ccxt')
    , main = require('./index.js') // for commandsMapping
    , puppeteer = require('puppeteer')
    , fs = require('fs')

let chartWidth = 1200
  , chartHeight = 600
  , bitfinexMarginPositionInfo = {}

function formatTicker(td, exchangeName){
    let msgBody =  `
*${exchangeName} | ${td.symbol} *
\`last:\`  *${td.last}* 
\`\`\`
high: ${td.high}
low:  ${td.low}
bid:  ${td.bid}
ask:  ${td.ask}\`\`\`
`  
    return msgBody
}

async function puppeteerChart(data){    
    const browser = await puppeteer.launch({ defaultViewport: { width: chartWidth, height: chartHeight } } )
    const page = await browser.newPage()
    await page.addScriptTag({ url: 'https://unpkg.com/lightweight-charts/dist/lightweight-charts.standalone.production.js' })
    await page.addScriptTag({ path: 'lib/chart.js' })
    await page.evaluate((chartData) => createLightweightChart(chartData), data)
    return await page.screenshot({ fullPage: true })
}

function respondToChart(ctx){
    ctx.replyWithChatAction('typing')
    try {
        let words = ctx.message.text.split(' ')
            ,exc = words[1].toLowerCase()
            ,sym = words[2]
        
        let exchange_constructor = ccxt[exc]
        if(typeof exchange_constructor === 'function'){
            let exchange = new exchange_constructor
            if (exchange.has.fetchOHLCV) {
                if(sym === undefined){
                    console.log('no symbol provided')
                    ctx.reply('no symbol provided')
                } else {
                    sym = sym.toUpperCase()
                    exchange.fetchOHLCV(sym, '1h', Date.now() - 1000 * 60 * 60 * 24 * 4)
                        .then((result) => {
                            console.log ('total of ' + result.length + ' rows received')
                            
                            let objectified = []

                            for(t in result){
                                let tt = result[t]
                                let item = {
                                    time: tt[0]/1000,
                                    open: tt[1],
                                    high: tt[2],
                                    low: tt[3],
                                    close: tt[4],
                                    volume: tt[5]
                                }
                                objectified.push(item)
                            }

                            puppeteerChart(objectified).then((chartImage) => {
                                console.log('got chart image data')
                                ctx.replyWithPhoto({ source: chartImage })
                            })

                        }, (failed) => {
                            console.log(failed)
                            ctx.replyWithMarkdown("Failed to get ticker data.\n```"+ failed +"```")
                        })
                }
            } else {
                ctx.reply('Error: ' + exc + ' has no fetchTicker method.')
            }
        } else {
            ctx.reply('Unsupported or unknown exchange "' + exc + '"' )
        }
    } catch(e){
        console.log('error')
        console.log(e)
        ctx.replyWithMarkdown("Error: ```" + e + "```" )
    }
}

function respondToTicker(ctx){
    ctx.replyWithChatAction('typing')
    try {
        // expects messages in the form: /ticker bitmex BTC/USD
        let words = ctx.message.text.split(' ')
        
        if(words.length == 1){
            reply = `No exchange specified.\n\n` +
                    `*Available Exchanges*\n` +
                    `This list shows all the exchanges supported by \`ccxt\`, the library used by this bot to access exchange APIs.\n` +
                    `Not all exchanges support all functions.\n` +
                    `-----------------------\n` +
                    `\\${ccxt.exchanges.join('\n')}`

            ctx.replyWithMarkdown(reply)
        } else {
            let exc = words[1].toLowerCase()
               ,sym = words[2]

            let exchange_constructor = ccxt[exc]
            
            if(typeof exchange_constructor === 'function'){
                let exchange =  new exchange_constructor
                if (exchange.has.fetchTicker) {
                    if(sym === undefined){
                        exchange.fetchMarkets()
                            .then((markets) => {
                                let list = "*Available " + exc + " tickers:*"
                                for(m in markets){
                                    list += "\n" + markets[m].symbol
                                }
                                ctx.replyWithMarkdown(list)
                            }, (failed) => {
                                let msg = 'Something went wrong fetching markets'
                                console.log(msg)
                                ctx.replyWithMarkdown("Error fetching markets:\n```" + msg + "```")
                            })
                    } else {
                        exchange.fetchTicker(sym)
                            .then((result) => {
                                // console.log (result)
                                ctx.replyWithMarkdown(formatTicker(result, exchange.name))
                            }, (failed) => {
                                console.log(failed)
                                ctx.replyWithMarkdown("Failed to get ticker data.\n```"+ failed +"```")
                            })
                    }
                } else {
                    ctx.reply('Error: ' + exc + ' has no fetchTicker method.')
                }
            } else {
                ctx.reply('Unsupported or unknown exchange "' + exc + '"' )
            }
        }
    } catch(e){
        console.log('error')
        console.log(e)
        ctx.replyWithMarkdown("Error: ```" + e + "```" )
    }
}

function showHelp(ctx){
    const commands = main.commandsMapping
    const keys = Object.keys(commands)
    
    let reply = 
        `*The following commands and features are available:*\n` +
        `-------------------------------------------------------\n`
    for(c in keys){
        let com = keys[c]
        let command = commands[com]
        reply = reply + `*${command.helpTitle}*\n${command.helpText}\n\n`
    }
    
    ctx.replyWithMarkdown(reply)
}

function bitFinexLongShort(ctx){
    ctx.replyWithChatAction('typing')
    console.log('bitfinexLongShot')
    console.log(bitfinexMarginPositionInfo.lastUpdated )
    console.log(Date.now() - 1000 * 60 * 2)
    if(bitfinexMarginPositionInfo.lastUpdated && bitfinexMarginPositionInfo.lastUpdated > Date.now() - 1000 * 60 * 5 /* 5 minutes */ ){        
        // serve from 'cache'
        console.log('using cache')
        console.log(bitfinexMarginPositionInfo)
        ctx.replyWithMarkdown(bitfinexMarginPositionInfo.message)
    } else {
        ctx.reply('Need to refresh cache. This will take a while.')
        ctx.replyWithChatAction('typing')
        getBitFinexLongShort()
            .then((reply) =>  {
                console.log('got response from bfx longshort function')
                console.log(reply)
                bitfinexMarginPositionInfo.message = reply
                bitfinexMarginPositionInfo.lastUpdated = Date.now()
                ctx.replyWithMarkdown(reply)
            })
    }
    // getBitFinexLongShort().then((reply) =>  ctx.replyWithMarkdown(reply))
}

async function getBitFinexLongShort(){
    let sleep = (ms) => new Promise (resolve => setTimeout (resolve, ms))
    
    const bfx = new ccxt.bitfinex
    // lazy and just repeating code
    // BTC, ETH LTC, BCH, 
    await sleep (bfx.rateLimit)
    let btcUsdLongs = await bfx.v2GetStats1KeySizeSymbolLongLast({'key': 'pos.size', 'size': '1m', 'symbol': 'tBTCUSD'})
    await sleep (bfx.rateLimit)
    let btcUsdShorts = await bfx.v2GetStats1KeySizeSymbolShortLast({'key': 'pos.size', 'size': '1m', 'symbol': 'tBTCUSD'})

    await sleep (bfx.rateLimit)
    let ethUsdLongs = await bfx.v2GetStats1KeySizeSymbolLongLast({'key': 'pos.size', 'size': '1m', 'symbol': 'tETHUSD'})
    await sleep (bfx.rateLimit)  
    let ethUsdShorts = await bfx.v2GetStats1KeySizeSymbolShortLast({'key': 'pos.size', 'size': '1m', 'symbol': 'tETHUSD'})
    await sleep (bfx.rateLimit)
    let ethBtcLongs = await bfx.v2GetStats1KeySizeSymbolLongLast({'key': 'pos.size', 'size': '1m', 'symbol': 'tETHBTC'})
    await sleep (bfx.rateLimit)
    let ethBtcShorts = await bfx.v2GetStats1KeySizeSymbolShortLast({'key': 'pos.size', 'size': '1m', 'symbol': 'tETHBTC'})
    await sleep (bfx.rateLimit)
    let eosUsdLongs = await bfx.v2GetStats1KeySizeSymbolLongLast({'key': 'pos.size', 'size': '1m', 'symbol': 'tEOSUSD'})
    await sleep (bfx.rateLimit)
    let eosUsdShorts = await bfx.v2GetStats1KeySizeSymbolShortLast({'key': 'pos.size', 'size': '1m', 'symbol': 'tEOSUSD'})
    await sleep (bfx.rateLimit)
    let eosBtcLongs = await bfx.v2GetStats1KeySizeSymbolLongLast({'key': 'pos.size', 'size': '1m', 'symbol': 'tEOSBTC'})
    await sleep (bfx.rateLimit)
    let eosBtcShorts = await bfx.v2GetStats1KeySizeSymbolShortLast({'key': 'pos.size', 'size': '1m', 'symbol': 'tEOSBTC'})
    await sleep (bfx.rateLimit)
    let ltcUsdLongs = await bfx.v2GetStats1KeySizeSymbolLongLast({'key': 'pos.size', 'size': '1m', 'symbol': 'tLTCUSD'})
    await sleep (bfx.rateLimit)
    let ltcUsdShorts = await bfx.v2GetStats1KeySizeSymbolShortLast({'key': 'pos.size', 'size': '1m', 'symbol': 'tLTCUSD'})
    await sleep (bfx.rateLimit)
    let ltcBtcLongs = await bfx.v2GetStats1KeySizeSymbolLongLast({'key': 'pos.size', 'size': '1m', 'symbol': 'tLTCBTC'})
    await sleep (bfx.rateLimit)
    let ltcBtcShorts = await bfx.v2GetStats1KeySizeSymbolShortLast({'key': 'pos.size', 'size': '1m', 'symbol': 'tLTCBTC'})
    await sleep (bfx.rateLimit)
    let bchUsdLongs = await bfx.v2GetStats1KeySizeSymbolLongLast({'key': 'pos.size', 'size': '1m', 'symbol': 'tBCHUSD'})
    await sleep (bfx.rateLimit)
    let bchUsdShorts = await bfx.v2GetStats1KeySizeSymbolShortLast({'key': 'pos.size', 'size': '1m', 'symbol': 'tBCHUSD'})
    await sleep (bfx.rateLimit)
    let bchBtcLongs = await bfx.v2GetStats1KeySizeSymbolLongLast({'key': 'pos.size', 'size': '1m', 'symbol': 'tBCHBTC'})
    await sleep (bfx.rateLimit)
    let bchBtcShorts = await bfx.v2GetStats1KeySizeSymbolShortLast({'key': 'pos.size', 'size': '1m', 'symbol': 'tBCHBTC'})
    await sleep (bfx.rateLimit)
    let bsvUsdLongs = await bfx.v2GetStats1KeySizeSymbolLongLast({'key': 'pos.size', 'size': '1m', 'symbol': 'tBSVUSD'})
    await sleep (bfx.rateLimit)
    let bsvUsdShorts = await bfx.v2GetStats1KeySizeSymbolShortLast({'key': 'pos.size', 'size': '1m', 'symbol': 'tBSVUSD'})
    await sleep (bfx.rateLimit)
    let bsvBtcLongs = await bfx.v2GetStats1KeySizeSymbolLongLast({'key': 'pos.size', 'size': '1m', 'symbol': 'tBSVBTC'})
    await sleep (bfx.rateLimit)
    let bsvBtcShorts = await bfx.v2GetStats1KeySizeSymbolShortLast({'key': 'pos.size', 'size': '1m', 'symbol': 'tBSVBTC'})

    // totals
    let longsBtc = parseFloat(btcUsdLongs[1])
      , shortsBtc = parseFloat(btcUsdShorts[1])
      , longsEth = parseFloat(ethUsdLongs[1]) + parseFloat(ethBtcLongs[1])
      , shortsEth = parseFloat(ethUsdShorts[1]) + parseFloat(ethBtcShorts[1])
      , longsEos = parseFloat(eosUsdLongs[1]) + parseFloat(eosBtcLongs[1])
      , shortsEos = parseFloat(eosUsdShorts[1]) + parseFloat(eosBtcShorts[1])
      , longsLtc = parseFloat(ltcUsdLongs[1]) + parseFloat(ltcBtcLongs[1])
      , shortsLtc = parseFloat(ltcUsdShorts[1]) + parseFloat(ltcBtcShorts[1])
      , longsBch = parseFloat(bchUsdLongs[1]) + parseFloat(bchBtcLongs[1])
      , shortsBch = parseFloat(bchUsdShorts[1]) + parseFloat(bchBtcShorts[1])
      , longsBsv = parseFloat(bsvUsdLongs[1]) + parseFloat(bsvBtcLongs[1])
      , shortsBsv = parseFloat(bsvUsdShorts[1]) + parseFloat(bsvBtcShorts[1])

    // percentages
    let pcBtcLongs = longsBtc / (longsBtc + shortsBtc)
      , pcBtcShorts = shortsBtc / (longsBtc + shortsBtc)
      , pcEthLongs = longsEth / (longsEth + shortsEth)
      , pcEthShorts = shortsEth / (longsEth + shortsEth)
      , pcEosLongs = longsEos / (longsEos + shortsEos)
      , pcEosShorts = shortsEos / (longsEos + shortsEos)
      , pcLtcLongs = longsLtc / (longsLtc + shortsLtc)
      , pcLtcShorts = shortsLtc / (longsLtc + shortsLtc)
      , pcBchLongs = longsBch / (longsBch + shortsBch)
      , pcBchShorts = shortsBch / (longsBch + shortsBch)
      , pcBsvLongs = longsBsv / (longsBsv + shortsBsv)
      , pcBsvShorts = shortsBsv / (longsBsv + shortsBsv)

    
    // write out the reply
    return `*Bitfinex Margin Positions*\n` +
    `--------------------------------\n` +
    `*BTC/USD*\n` +
    `--------------------------------\n` +
    `Longs: *${Math.round(pcBtcLongs*1000)/10}%* | *${(Math.round(longsBtc*10)/10).toLocaleString('en')} BTC*\n` +
    `Shorts: *${Math.round(pcBtcShorts*1000)/10}%* | *${(Math.round(shortsBtc*10)/10).toLocaleString('en')} BTC*\n\n` +

    `*ETH* (ETH/USD + ETC/BTC)\n` +
    `--------------------------------\n` +
    `Longs: *${Math.round(pcEthLongs*1000)/10}%* | *${(Math.round(longsEth*10)/10).toLocaleString('en')} ETH*\n` +
    `Shorts: *${Math.round(pcEthShorts*1000)/10}%* | *${(Math.round(shortsEth*10)/10).toLocaleString('en')} ETH*\n\n` +

    `*EOS* (EOS/USD + EOS/BTC)\n` +
    `--------------------------------\n` +
    `Longs: *${Math.round(pcEosLongs*1000)/10}%* | *${(Math.round(longsEos*10)/10).toLocaleString('en')} EOS*\n` +
    `Shorts: *${Math.round(pcEosShorts*1000)/10}%* | *${(Math.round(shortsEos*10)/10).toLocaleString('en')} EOS*\n\n` +
  
    `*LTC* (LTC/USD + LTC/BTC)\n` +
    `--------------------------------\n` +
    `Longs: *${Math.round(pcLtcLongs*1000)/10}%* | *${(Math.round(longsLtc*10)/10).toLocaleString('en')} LTC*\n` +
    `Shorts: *${Math.round(pcLtcShorts*1000)/10}%* | *${(Math.round(shortsLtc*10)/10).toLocaleString('en')} LTC*\n\n` +
  
    `*BCH* (BCH/USD + BCH/BTC)\n` +
    `--------------------------------\n` +
    `Longs: *${Math.round(pcBchLongs*1000)/10}%* | *${(Math.round(longsBch*10)/10).toLocaleString('en')} BCH*\n` +
    `Shorts: *${Math.round(pcBchShorts*1000)/10}%* | *${(Math.round(shortsBch*10)/10).toLocaleString('en')} BCH*\n\n` +
  
    `*BSV* (BSV/USD + BSV/BTC)\n` +
    `--------------------------------\n` +
    `Longs: *${Math.round(pcBsvLongs*1000)/10}%* | *${(Math.round(longsBsv*10)/10).toLocaleString('en')} BSV*\n` +
    `Shorts: *${Math.round(pcBsvShorts*1000)/10}%* | *${(Math.round(shortsBsv*10)/10).toLocaleString('en')} BSV*\n\n`                 
  
       
}

module.exports = {
    bitFinexLongShort,
    respondToChart,
    respondToTicker,
    showHelp
}

