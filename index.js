const Telegraf = require('telegraf')
const ccxt = require ('ccxt')
const fs = require('fs')
const { bitFinexLongShort, respondToChart, respondToTicker, showHelp} = require('./func')

const img_dir = 'tiffimg'
const tiffs = fs.readdirSync(img_dir)
console.log(`loaded ${tiffs.length} tiff images`)

const bot = new Telegraf(process.env.BOT_TOKEN)

bot.hears(
    /.*\btiff(any)?\b.*/gmi, 
    (ctx) => {
        console.log(`heard 'tiff' from ${ctx.from.first_name}`)
        ctx.replyWithPhoto({
            source: fs.createReadStream( img_dir + '/' + tiffs[Math.floor(Math.random()*tiffs.length)])
        })
    }
)

const commandsMapping = {
    bitfinex_longshort : {
        handler: bitFinexLongShort,
        helpTitle: "Bitfinex long/shorts",
        helpText: "/bitfinex\\_longshort\nShow Bitfinex long/short positions information"
    },
    ticker: {
        handler: respondToTicker,
        helpTitle: 'Show Ticker',
        helpText: '/ticker _exchange market_\ne.g. `/ticker bitmex BTC/USD`',
        params: ['exchange', 'market']
    },
    chart: {
        handler: respondToChart,
        helpTitle: 'Show Chart',
        helpText: '/chart _exchange market_\nshow chart, e.g. `/chart bitmex BTC/USD`',
        params: ['exchange', 'market']
    },
    help: {
        handler: showHelp,
        helpTitle: 'Show Commands',        
        helpText:  '/help\ndisplay this message.'        
    }
}

bot.start((ctx) => {
    console.log(`bot started by ${ctx.from.first_name}`)
    showHelp(ctx)
})

const commands = Object.keys(commandsMapping)

for(i in commands){
    c = commands[i]
    h = commandsMapping[c].handler
    bot.command(c, h)
}

bot.launch()

exports.commandsMapping = commandsMapping

// FOMO Bot commands:
// BitFinex
// /bitfinex_longshort - grab positions of Finex long vs short { BTC ☑️ }
// /bitfinex_swaprates - grab swaprates of lent Bitfinex assets
// /bitfinex_marginuse - grab the USD and BTC lent for margin in BTCUSD

// Listed Bitcoin Futures
// /cme_futures Grab the price, vol, and open interest for the CME front contract
// /cboe_futures grab the price and volume on the Cboe front contract

// OKex
// /okex_futures_volume grab OKex futures volume on their three maturities
// /okex_futures_openinterest grab OKex futures open interest on their three maturities
// /okex_futures_premium grab OKex futures on their three maturities
// /okex_futures_margin - grabs OKex futures margin usage data between longs and shorts
// /okex_futures_elite_sentiment - grabs OKex futures elite trader sentiment
// /okex_futures_topholders - grabs OKex Futures top holder positions
// /okex_futures_settlement - countdown to OKex Futures settlement 

// Deribit
// /deribit_futures_volume grab Deribit futures volume on their three maturities
// /deribit_futures_openinterest grab Deribit futures open interest on their three maturities
// /deribit_futures_premium grab Deribit futures on their three maturities

// Crypto Facilities
// /kf_futures_volume grab CryptoFacilities futures volume on their three maturities
// /kf_futures_openinterest grab CryptoFacilities futures open interest on their three maturities
// /kf_futures_premium grab CryptoFacilities futures on their three maturities

// BitMEX
// /bitmex_funding grab the next BitMEX XBTUSD and ETHUSD perpetual swap funding data
// /bitmex_futures_volume grab BitMEX futures volume 
// /bitmex_futures_openinterest grab BitMEX futures open interest on their futures
// /bitmex_futures_premium grab BitMEX futures premiums

// Other
// /exchanges See a list of reliable exchanges recommended by top traders