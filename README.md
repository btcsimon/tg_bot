# TG bot experiments
Just some code playing around with a Telegram bot to do some stuff with crypto data.

## Installing
You need NodeJS installed and npm working first; then in the project directory run npm to install dependencies:

`npm install`

Chromium is used for browser screenshots and might take a while to download.

## Running
You need to create a bot in Telegram via the @BotFather bot. It will give you a token. The code reads the token from an environment variable so you need to do something like this before running:
`export BOT_TOKEN=&77GHJkKHJghIUtiK<gjKUykj`

You can also add the above command to your `./bash_profile` or similar so it's run automatically when you login.

Run the bot:
`node index`

## Webserver
Because I've been playing with HTML/JS/SVG/CSS it's handy to be able to open them in a browser. `server.js` is a basic static file server that just serves the whole project directory on http://localhost:3000 - run it with `node server`

You probably don't want to deploy this.